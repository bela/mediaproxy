#!/bin/bash
# this is just a helper since test.sh isn't available to gitlab ci.

pipenv run pyflakes run.py mediaproxy tests && pipenv run pytest tests
