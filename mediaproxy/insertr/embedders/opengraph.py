# mediaproxy: mediaproxy component of litecord
# Copyright 2018-2019, Luna Mendes and the mediaproxy contributors
# SPDX-License-Identifier: AGPL-3.0-only

from bs4 import BeautifulSoup
from logbook import Logger

from mediaproxy.utils import embed_image
from mediaproxy.bp.url_cache import ucache_get_or_req

log = Logger(__name__)

async def inject_opengraph_image(embed, meta_content: str):
    """Inject embed.thumbnail based on an og:image meta tag"""
    image = await embed_image(meta_content)

    if image:
        embed['thumbnail'] = image


async def from_html(html: str) -> dict:
    """Generate an embed out of raw HTML information."""
    # NOTE: the lxml html parser is much better than python's.
    # i was having trouble understanding its meta tags on python.
    soup = BeautifulSoup(html, 'lxml')

    embed = {}
    head = soup.head

    if not head:
        return {}

    for child in head.children:
        # skip non-<meta>
        if child.name != 'meta':
            continue

        try:
            meta_prop = child['property']
            meta_content = child['content']
        except KeyError:
            continue

        # if we have at least one meta tag, set embed type to url
        embed['type'] = 'url'

        if meta_prop == 'og:title':
            embed['title'] = meta_content

        if meta_prop == 'og:description':
            embed['description'] = meta_content

        if meta_prop == 'og:image':
            await inject_opengraph_image(embed, meta_content)

    return embed


async def gen_embed(url: str) -> dict:
    """Generate an embed based on OpenGraph data. Requests the HTML off
    the page and feeds to the from_html function."""
    text = await ucache_get_or_req(url)
    embed = await from_html(text)

    if embed:
        # inject embed.url
        embed['url'] = url

    return embed
