# mediaproxy

Proxy Images and other files through itself. Also can generate Discord Embeds
out of given websites, via OpenGraph or `<meta>` tags or other embedders.

This is a component of the [Litecord] stack.

[litecord]: https://gitlab.com/litecord/litecord

Even though it is part of the stack, they are separated
and can be deployed on different machines. Your server's
origin IP will be "leaked" via running this software on it.

Discord does not use this software.

## Installation

Requirements:
 - **Python 3.7+**
 - [pipenv]

`mediaproxy` does not require a database. The filesystem is used as a cache
storage.

[pipenv]: https://github.com/pypa/pipenv

### Download the code

```sh
$ git clone https://gitlab.com/litecord/mediaproxy.git && cd mediaproxy
```

### Install packages

**NOTE:** Production deployments do NOT require `--dev`.

```sh
$ pipenv install --dev
```

### Configuration

```sh
$ cp config.example.ini config.ini
# edit config.ini as wanted
```

### Running

It is preferred to run mediaproxy on the port `5002`,
while Litecord and its websocket run on ports 5000 and 5001 respectively.

```sh
$ pipenv run hypercorn run:app --bind 0.0.0.0:5002
```

**It is recommended to run mediaproxy behind [NGINX].** You can use the
`nginx.conf` file at the root of the repository as a template.

[nginx]: https://www.nginx.com

## Usage

external websites use the following syntax in paths:

```
/<scheme>/<path>
```

`path` is composed of `<host>/<url_path>`.
`scheme` can only be `http` or `https`.

 - the `/img/` scope downloads an external website and depending on the mimetype
    it'll respond with the original body. useful for images or general
    multimedia urls

 - the `/meta` scope gives metadata about the given image (if it is an image),
    such as width and height, used for embed generation in litecord (since
    it would be inefficient to downlaod the image in litecord and do
    processing there).

 - the `/embed/` scope gives a discord embed representing the url. depending
    of the url's format it can be routed to a different embedder, such as
    the OGP (OpenGraph Protocol) embedder, `<meta>` embedder, or XKCD embedder,
    etc. sites must reply with `text/html` to be valid.

examples:

if you want to get a certain image's raw data as a response body,
`http://blah.com/img/http/website.com/photo.png`

replace `img` with `meta` for metadata, and `img` with `embed` to generate
an embed.
