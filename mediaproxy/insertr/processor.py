# mediaproxy: mediaproxy component of litecord
# Copyright 2018-2019, Luna Mendes and the mediaproxy contributors
# SPDX-License-Identifier: AGPL-3.0-only

import re
import json

from logbook import Logger

from mediaproxy.cache import CACHE_DIR

from mediaproxy.insertr.embedders import (
    xkcd_embed, opengraph_embed, meta_embed
)

log = Logger(__name__)

EMBEDDERS = {
    re.compile(r'xkcd.com\/\d+'): xkcd_embed,
    '': [opengraph_embed, meta_embed],
}


async def _multi_embedders(embedders, url):
    if not isinstance(embedders, list):
        return await embedders(url)

    # iterate over all embedders and return the
    # first one that has a good embed
    for embedder in embedders:
        log.debug('processing {!r} with {!r}', url, embedder)
        embed = await embedder(url)

        if not embed:
            continue

        keys = embed.keys()

        # invalid when the embed only has a single key,
        # and that key is the type key.
        invalid = len(keys) == 1 and 'type' in keys

        if embed and not invalid:
            return embed

    return None


async def _process_url(url):
    # NOTE: its named embedders because in the future we plan to have
    # multiple fallback embedders.

    for regex, embedders in EMBEDDERS.items():
        # TODO: support for multiple fallback embedders
        # TODO: keep the HTML bodies of the urls in an IN-MEMORY cache, so
        # embedders can share those bodies.

        # if regex is empty, then we are on a fallback for
        # any kind of url.
        if not regex:
            return await _multi_embedders(embedders, url)

        # if it is a regex, check if it matches (anywhere in the string,
        # with re.search)
        if regex.search(url):
            return await embedders(url)


async def process_url(url, url_id: str):
    """Process the given url's embed and store it in the given urlid as a
    JSON file."""

    # pass the url through the regexes
    embed = await _process_url(url)

    embed_path = CACHE_DIR / f'{url_id}.json'

    # if the embedder returns empty embed, then we should
    # treat it as no embed at all.
    embed_path.write_text(json.dumps(embed or None))

    return embed_path
